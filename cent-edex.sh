#!/bin/bash

#### CentOS 8 with openbox and eDEX-UI

# EPEL & ELRepo
sudo dnf -y install elrepo-release epel-release

# RPM Fusion
sudo dnf config-manager --enable PowerTools
sudo dnf -y install --nogpgcheck https://download1.rpmfusion.org/free/el/rpmfusion-free-release-8.noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-8.noarch.rpm
sudo dnf -y install rpmfusion-free-release-tainted
sudo dnf -y install https://pkgs.dyn.su/el8/base/x86_64/raven-release-1.0-1.el8.noarch.rpm

# eDEX-UI installation
sudo dnf -y install adwaita-gtk2-theme dbus-x11 gtk3 fuse i3lock ntfs-3g openbox p7zip qgnomeplatform xorg-x11-server-Xorg xorg-x11-xinit zenity zip unrar unzip
mkdir -p ~/.config/openbox
mkdir -p ~/.local/share/edex-ui
mkdir ~/.themes
wget https://github.com/GitSquared/edex-ui/releases/download/v2.2.2/eDEX-UI.Linux.x86_64.AppImage -P ~/.local/share/edex-ui
unzip Adwaita_Revisited_Dark.zip -d ~/.themes
mv rc.xml ~/.config/openbox
echo '[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx' >> ~/.bash_profile
echo '#!/bin/sh' > ~/.xinitrc
echo 'exec dbus-launch openbox-session' >> ~/.xinitrc
echo sh -c "setxkbmap -layout us,ru -variant winkeys -option 'grp:alt_shift_toggle,grp_led:scroll,terminate:ctrl_alt_bksp,compose:ralt'" > ~/.config/openbox/autostart
echo 'exec ~/.local/share/edex-ui/eDEX-UI.Linux.x86_64.AppImage &' >> ~/.config/openbox/autostart
echo "export QT_QPA_PLATFORMTHEME='gnome'" >> ~/.bashrc
chmod +x ~/.xinitrc
chmod +x ~/.local/share/edex-ui/eDEX-UI.Linux.x86_64.AppImage
chmod +x ~/.config/openbox/autostart

# firefox
sudo dnf -y install firefox