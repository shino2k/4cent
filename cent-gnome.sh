#!/bin/bash

#### CentOS 8 without cheese and other shit

## my network settings

#echo 'nameserver 192.168.1.230' >> /etc/resolv.conf
#echo 'domain saturn.net' >> /etc/resolv.conf
#chattr +i /etc/resolv.conf

## additional repos

# EPEL & ELRepo
dnf -y install elrepo-release epel-release

# RPM Fusion
dnf config-manager --enable PowerTools
dnf -y install --nogpgcheck https://download1.rpmfusion.org/free/el/rpmfusion-free-release-8.noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-8.noarch.rpm
dnf -y install rpmfusion-free-release-tainted
dnf -y groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
dnf -y groupupdate sound-and-video

# CERT Forensics Tools
sudo dnf -y install https://forensics.cert.org/cert-forensics-tools-release-el8.rpm
sudo dnf -y install CERT-Forensics-Tools 

# Flathub
dnf -y install flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

## GNOME minimal app set
dnf -y install \
adwaita-gtk2-theme \
chrome-gnome-shell \
dejavu-* \
gnome-shell \
gnome-software \
gnome-terminal \
gnome-terminal-nautilus \
google-noto-sans-cjk-ttc-fonts \
google-noto-serif-cjk-ttc-fonts \
kmod-sky2 \
nautilus nautilus-sendto \
NetworkManager-adsl \
NetworkManager-l2tp-gnome \
NetworkManager-libreswan-gnome \
NetworkManager-openconnect-gnome \
NetworkManager-openvpn-gnome \
NetworkManager-pptp-gnome \
p7zip \
qgnomeplatform \
unrar \
usb_modeswitch \

flatpak -y install flathub org.gnome.Extensions

## Additional software
dnf -y install deadbeef evolution file-roller filezilla firewall-config gnome-font-viewer htop mpv qbittorrent
flatpak -y install flathub org.speedcrunch.SpeedCrunch

# AnyDesk
#flatpak -y install flathub com.anydesk.Anydesk

# Etcher
wget https://balena.io/etcher/static/etcher-rpm.repo -O /etc/yum.repos.d/etcher-rpm.repo
dnf -y install balena-etcher-electron

# Firefox
dnf -y install firefox

# Okular
#flatpak -y install flathub org.kde.okular

# Spotify
flatpak -y install flathub com.spotify.Client

# Sublime Merge
#flatpak -y install flathub com.sublimemerge.App

# Telegram
flatpak -y install flathub org.telegram.desktop

# VSCode
rpm --import https://packages.microsoft.com/keys/microsoft.asc
sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
dnf -y install code

# Wireguard
dnf -y install kmod-wireguard wireguard-tools

# ZSH
dnf -y install zsh

# XnView
flatpak -y install flathub com.xnview.XnViewMP

systemctl set-default graphical.target
